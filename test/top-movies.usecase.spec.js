import { MoviesRepository } from '../src/repositories/movies.repository';
import { TopMoviesUseCase } from '../src/usecases/top-movies.usecase';

import { MOVIES } from './fixtures/movies';

jest.mock('../src/repositories/movies.repository');

describe('top movies use case', () => {
  beforeEach(() => {
    MoviesRepository.mockClear();
  });

  it('should get all top movies', async () => {
    MoviesRepository.mockImplementation(() => {
      return {
        getTopMovies: () => {
          return MOVIES;
        },
      };
    });

    const movies = await TopMoviesUseCase.execute();

    expect(movies.length).toBe(20);
    expect(movies.length).toBe(MOVIES.length);
    expect(movies[0].title).toBe('The Godfather');
    expect(movies[19].title).toBe('GoodFellas');
    expect(movies[0].language).toBe(MOVIES[0].original_language);
  });
});
