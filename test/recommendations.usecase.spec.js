import { MoviesRepository } from '../src/repositories/movies.repository';
import { RecommendationsUseCase } from '../src/usecases/recommendations.usecase';

import { RECOMMENDATIONS } from './fixtures/recommendations';

jest.mock('../src/repositories/movies.repository');

describe('recommendations use case', () => {
  beforeEach(() => {
    MoviesRepository.mockClear();
  });

  it('should get movie recommendations by id', async () => {
    MoviesRepository.mockImplementation(() => {
      return {
        getRecommendations: () => {
          return RECOMMENDATIONS;
        },
      };
    });

    const theGodfatherId = 238;
    const recommendations = await RecommendationsUseCase.execute(
      theGodfatherId
    );

    expect(recommendations.length).toBe(21);
    expect(recommendations[0].title).toBe('Cuando Sea Joven');
  });
});
