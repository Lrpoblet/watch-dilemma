import { MoviesRepository } from '../src/repositories/movies.repository';
import { SearchByNameUseCase } from '../src/usecases/search-by-name.usecase';

jest.mock('../src/repositories/movies.repository');

describe('search by title use case', () => {
  beforeEach(() => {
    MoviesRepository.mockClear();
  });

  it('should get movies searching by name', async () => {
    const STARWARS_RESULTS = [
      {
        id: 11,
        title: 'La guerra de las galaxias',
        date: '1977-05-25',
        overview: 'La guerra de las galaxias',
        poster: '/ahT4ObS7XKedQkOSpGr1wQ97aKA.jpg',
        score: 8.208,
        backgroundImage: '/2w4xG178RpB4MDAIfTkqAuSJzec.jpg',
        genre: [12, 28, 878],
        language: 'en',
      },
      {
        id: 181808,
        title: 'Star Wars: Los últimos Jedi',
        date: '2017-12-13',
        overview: 'Star Wars: Los últimos Jedi',
        poster: '/rjBwhsOzHKUw2NIOrE7aMqjfe6s.jpg',
        score: 6.8,
        backgroundImage: '/5Iw7zQTHVRBOYpA0V6z0yypOPZh.jpg',
        genre: [12, 28, 878],
        language: 'en',
      },
      {
        id: 140607,
        title: 'Star Wars: El despertar de la fuerza',
        date: '2015-12-15',
        overview: 'Star Wars: El despertar de la fuerza',
        poster: '/vI86GLIkT5pP3ZfNs2EvmTrBLBm.jpg',
        score: 7.3,
        backgroundImage: '/8BTsTfln4jlQrLXUBquXJ0ASQy9.jpg',
        genre: [12, 28, 878, 14],
        language: 'en',
      },
      {
        id: 181812,
        title: 'Star Wars: El ascenso de Skywalker',
        date: '2019-12-18',
        overview: 'Star Wars: El ascenso de Skywalker',
        poster: '/16G2wZAkmKqSGK3it2VPjco5oyn.jpg',
        score: 6.4,
        backgroundImage: '/jOzrELAzFxtMx2I4uDGHOotdfsS.jpg',
        genre: [12, 28, 878],
        language: 'en',
      },
      {
        id: 348350,
        title: 'Han Solo: Una historia de Star Wars',
        date: '2018-05-15',
        overview: 'Han Solo: Una historia de Star Wars',
        poster: '/isFKKZS3JEIxGfcciL2DQEYhiy1.jpg',
        score: 6.563,
        backgroundImage: '/ojHCeDULAkQK25700fhRU75Tur2.jpg',
        genre: [878, 12, 28],
        language: 'en',
      },
      {
        id: 330459,
        title: 'Rogue One: Una historia de Star Wars',
        date: '2016-12-14',
        overview: 'Rogue One: Una historia de Star Wars',
        poster: '/mAqgFQxaBaLkcQBRQf9YnAz9sNQ.jpg',
        score: 7.487,
        backgroundImage: '/6t8ES1d12OzWyCGxBeDYLHoaDrT.jpg',
        genre: [28, 12, 878],
        language: 'en',
      },
      {
        id: 12180,
        title: 'Star Wars: Las guerras clon',
        date: '2008-08-05',
        overview: 'Star Wars: Las guerras clon',
        poster: '/d6YOfi0T9GowglzIkDQGGvGYVTM.jpg',
        score: 6.121,
        backgroundImage: '/xEdnGUc8KeZ7h0eEgSN3ERhjgL0.jpg',
        genre: [16, 28, 878, 12],
        language: 'en',
      },
      {
        id: 857702,
        title: 'LEGO Star Wars Cuentos escalofriantes',
        date: '2021-10-01',
        overview: 'LEGO Star Wars Cuentos escalofriantes',
        poster: '/p8cPUCexDTFZxHI3ooqHYi6AUgL.jpg',
        score: 6.8,
        backgroundImage: '/f53Jujiap580mgfefID0T0g2e17.jpg',
        genre: [16, 10751, 878, 35, 28, 27, 10770],
        language: 'en',
      },
      {
        id: 980804,
        title: 'LEGO Star Wars: Vacaciones de verano',
        date: '2022-08-05',
        overview: 'LEGO Star Wars: Vacaciones de verano',
        poster: '/gT1Toend98eSyXL1PrL7ipz4dt.jpg',
        score: 6.067,
        backgroundImage: '/vV5knD9jlW8QaOhCgf4129hbIIh.jpg',
        genre: [10751, 16, 35, 878, 10770],
        language: 'en',
      },
      {
        id: 732670,
        title: 'LEGO Star Wars: Especial Felices Fiestas',
        date: '2020-11-17',
        overview: 'LEGO Star Wars: Especial Felices Fiestas',
        poster: '/ApYuiyKtkyA1SMC6UgvKZczPwUj.jpg',
        score: 6.659,
        backgroundImage: '/1Lhc32P0a62BgMFgd20wXR1osR3.jpg',
        genre: [16, 10751, 12, 35, 878, 28, 10770],
        language: 'en',
      },
      {
        id: 392216,
        title: 'Phineas y Ferb: Star Wars',
        date: '2014-07-26',
        overview: 'Phineas y Ferb: Star Wars',
        poster: '/xomphpz7MIasqVluPX83TjoTL8G.jpg',
        score: 7.06,
        backgroundImage: '/uNjBnOmdjZoiWTLQ938YJZ1cYVU.jpg',
        genre: [16, 35, 10751, 878, 12, 10770],
        language: 'en',
      },
      {
        id: 76180,
        title: 'El imperio de los sueños. La historia de Star Wars',
        date: '2004-09-12',
        overview: 'El imperio de los sueños. La historia de Star Wars',
        poster: '/xq0MPT1lLxfznTRaAbInxWT2wfh.jpg',
        score: 7.8,
        backgroundImage: '/11tVJLKi2PgcuNWqaCNPSOpINQx.jpg',
        genre: [99],
        language: 'en',
      },
      {
        id: 825647,
        title: 'Star Wars: BIOMAS',
        date: '2021-05-04',
        overview: 'Star Wars: BIOMAS',
        poster: '/qtNdZ9d6rVOdxUs1M3fG12II5ZO.jpg',
        score: 6.7,
        backgroundImage: '/7rZrvtpQTrA9B1nqoHNGjsp8j8i.jpg',
        genre: [878, 99],
        language: 'en',
      },
      {
        id: 42979,
        title: 'Robot Chicken: Star Wars Episodio I',
        date: '2007-07-17',
        overview: 'Robot Chicken: Star Wars Episodio I',
        poster: '/h44WN4mVJ6wEpJgLaaNoFjv0NAo.jpg',
        score: 7.248,
        backgroundImage: null,
        genre: [16, 35, 878],
        language: 'en',
      },
      {
        id: 74849,
        title:
          'El especial navideño de la Guerra de las Galaxias (The Star Wars Holiday Special)',
        date: '1978-12-01',
        overview:
          'El especial navideño de la Guerra de las Galaxias (The Star Wars Holiday Special)',
        poster: '/3vFFdLKigS3swN0pIBQbuRqSXIy.jpg',
        score: 3.362,
        backgroundImage: '/ae9xlnkS2qb5Dy9Mtlu68AWh42O.jpg',
        genre: [14, 878, 10751, 10770],
        language: 'en',
      },
      {
        id: 51888,
        title: 'Robot Chicken: Star Wars Episodio III',
        date: '2010-12-19',
        overview: 'Robot Chicken: Star Wars Episodio III',
        poster: '/mi2lVho2zpfwcxI6yC1QYJi435D.jpg',
        score: 7.4,
        backgroundImage: null,
        genre: [35, 878, 16, 10770],
        language: 'en',
      },
      {
        id: 128295,
        title: 'Star Wars: El legado',
        date: '2007-05-28',
        overview: 'Star Wars: El legado',
        poster: '/r8DHOCDpZagHDNPt0K3O82ppGKx.jpg',
        score: 7.132,
        backgroundImage: '/1Hhp0ys20V6I73Ph0MyhcDX2KOX.jpg',
        genre: [99],
        language: 'en',
      },
      {
        id: 70608,
        title: 'Lego Star Wars: La Amenaza Padawan',
        date: '2011-07-22',
        overview: 'Lego Star Wars: La Amenaza Padawan',
        poster: '/lhO0BRjmgWDlosXWHd1yEeSqwlb.jpg',
        score: 6.52,
        backgroundImage: '/hVl0qfLZ5loznDgQ0HxCTZqvZhz.jpg',
        genre: [10751, 16, 878],
        language: 'en',
      },
      {
        id: 782054,
        title: "Doraemon: Nobita's Little Star Wars 2021",
        date: '2022-03-04',
        overview: "Doraemon: Nobita's Little Star Wars 2021",
        poster: '/48gKZioIDeUOI0afbYv3kh9u9RQ.jpg',
        score: 5.825,
        backgroundImage: '/iflKt34Ck2JpY2PY9wW1zwdJgJi.jpg',
        genre: [16, 878, 12],
        language: 'ja',
      },
      {
        id: 378386,
        title: 'Star Wars: Greatest Moments',
        date: '2015-12-26',
        overview: 'Star Wars: Greatest Moments',
        poster: '/zIffPwISrW48qSmvAXEV27lBTMA.jpg',
        score: 6.85,
        backgroundImage: null,
        genre: [99],
        language: 'en',
      },
    ];

    MoviesRepository.mockImplementation(() => {
      return {
        searchByTitle: () => {
          return STARWARS_RESULTS;
        },
      };
    });

    const starWars = 'Star Wars';

    const searchResults = await SearchByNameUseCase.execute(starWars);

    expect(searchResults.length).toBe(20);
    expect(searchResults.length).toBe(STARWARS_RESULTS.length);
  });
});
