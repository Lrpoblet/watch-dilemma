import { GenreMoviesUseCase } from '../src/usecases/genre-movies.usecase';
import { MOVIES } from './fixtures/movies';

describe('genre movies use case', () => {
  it('should get movies by gender', async () => {
    const drama = await GenreMoviesUseCase.execute(MOVIES, 18);
    const crime = await GenreMoviesUseCase.execute(MOVIES, 80);
    const comedy = await GenreMoviesUseCase.execute(MOVIES, 35);
    const fantasy = await GenreMoviesUseCase.execute(MOVIES, 14);
    const history = await GenreMoviesUseCase.execute(MOVIES, 36);
    const war = await GenreMoviesUseCase.execute(MOVIES, 10752);
    const romance = await GenreMoviesUseCase.execute(MOVIES, 10749);
    const action = await GenreMoviesUseCase.execute(MOVIES, 28);
    const adventure = await GenreMoviesUseCase.execute(MOVIES, 12);
    const animation = await GenreMoviesUseCase.execute(MOVIES, 16);
    const documentary = await GenreMoviesUseCase.execute(MOVIES, 99);
    const horror = await GenreMoviesUseCase.execute(MOVIES, 27);
    const music = await GenreMoviesUseCase.execute(MOVIES, 10402);
    const mystery = await GenreMoviesUseCase.execute(MOVIES, 9648);
    const scienceFiction = await GenreMoviesUseCase.execute(MOVIES, 878);
    const TVMovie = await GenreMoviesUseCase.execute(MOVIES, 10770);
    const thriller = await GenreMoviesUseCase.execute(MOVIES, 53);
    const western = await GenreMoviesUseCase.execute(MOVIES, 37);

    expect(western[0].title).toBe('The Good, the Bad and the Ugly');
    expect(drama.length).toBe(12);
    expect(crime.length).toBe(7);
    expect(comedy.length).toBe(5);
    expect(fantasy.length).toBe(5);
    expect(history.length).toBe(1);
    expect(war.length).toBe(1);
    expect(romance.length).toBe(5);
    expect(action.length).toBe(2);
    expect(adventure.length).toBe(2);
    expect(animation.length).toBe(5);
    expect(documentary.length).toBe(0);
    expect(horror.length).toBe(0);
    expect(music.length).toBe(0);
    expect(mystery.length).toBe(0);
    expect(scienceFiction.length).toBe(0);
    expect(TVMovie.length).toBe(0);
    expect(thriller.length).toBe(3);
    expect(western.length).toBe(1);
  });
});
