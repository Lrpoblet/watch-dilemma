# Watch Dilemma

![Watch Dilemma logo](./src/images/logo2.png)

## Author

Lara R. Poblet - [@lrpoblet](https://lrpoblet.github.io/lara-ramos-poblet/)

## Abstract

Tired of wasting time browsing through endless movie catalogs? Feeling indecisive and not sure what to watch? Your solution has arrived! With this app, you can easily access the top-rated movies, as well as search for a movie you liked and discover other similar movie recommendations by clicking on it. Don't waste any more time, start searching and find your next favorite movie!

## Specifications

The project have navigable routes within the application.

- A home page showing the top rated movies and the search bar.
- The results of the user's search.
- Recommendations of movies related to which the user clicks.

The following technologies have been used in the development of this web application:

- HTML
- CSS
- Web components with Lit Element and @vaadin/router
- Event management in the browser
- Access data to a server
- Git for project version control
- Jest to make unit tests
- E2E testing with Cypress
- Deploy in firebase
- Publishing the result on the Internet using sonarcloud and firebase.

## Hosting

https://watch-dilemma-web.web.app/

## Docker image

`docker pull lrpoblet/watch-dilemma:2.0.1`

## API

The API from which the data has been obtained is:
https://developers.themoviedb.org/

## Other links of interest

- Favicon: https://icons8.com/
- Animation: https://www.joomla-monster.com/

## Getting Started

### Installation

Clone the repo:

`git clone https://gitlab.com/Lrpoblet/watch-dilemma`

Install NPM packages

`npm install`

### Usage

To run the app you just need to use the following command:

`npm run start`
