import { Movie } from '../model/movie';
import { MoviesRepository } from '../repositories/movies.repository';

export class SearchByNameUseCase {
  static async execute(title) {
    const repository = new MoviesRepository();

    const titleToLowerCase = title.toLowerCase();

    const movies = await repository.searchByTitle(titleToLowerCase);
    return movies.map(
      (movie) =>
        new Movie({
          id: movie.id,
          title: movie.title,
          date: movie.release_date,
          overview: movie.overview,
          poster: movie.poster_path,
          score: movie.vote_average,
          backgroundImage: movie.backdrop_path,
          genre: movie.genre_ids,
          language: movie.original_language,
        })
    );
  }
}
