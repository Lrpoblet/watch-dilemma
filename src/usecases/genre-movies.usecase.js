import { Movie } from '../model/movie';

export class GenreMoviesUseCase {
  static execute(movies, genreId) {
    const filteredMovies = movies.filter((movie) => {
      return movie.genre_ids.some((id) => id === genreId);
    });
    return filteredMovies.map((movie) => {
      return new Movie({
        id: movie.id,
        title: movie.title,
        date: movie.release_date,
        overview: movie.overview,
        poster: movie.poster_path,
        score: movie.vote_average,
        backgroundImage: movie.backdrop_path,
        genre: movie.genre_ids,
        language: movie.original_language,
      });
    });
  }
}
