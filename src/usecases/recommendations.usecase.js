import { Movie } from '../model/movie';
import { MoviesRepository } from '../repositories/movies.repository';

export class RecommendationsUseCase {
  static async execute(id) {
    const repository = new MoviesRepository();
    const movies = await repository.getRecommendations(id);
    return movies.map(
      (movie) =>
        new Movie({
          id: movie.id,
          title: movie.title,
          date: movie.release_date,
          overview: movie.overview,
          poster: movie.poster_path,
          score: movie.vote_average,
          backgroundImage: movie.backdrop_path,
          genre: movie.genre_ids,
          language: movie.original_language,
        })
    );
  }
}
