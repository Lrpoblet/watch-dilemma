import axios from 'axios';

export class MoviesRepository {
  async getTopMovies() {
    return await (
      await axios.get(
        'https://api.themoviedb.org/3/movie/top_rated?api_key=aed84e419fb70252f967303e3cf406d2&language=en-US&page=1'
      )
    ).data.results;
  }

  async getRecommendations(id) {
    return await (
      await axios.get(
        `https://api.themoviedb.org/3/movie/${id}/recommendations?api_key=aed84e419fb70252f967303e3cf406d2&language=en-US&page=1`
      )
    ).data.results;
  }

  async searchByTitle(title) {
    return await (
      await axios.get(
        `https://api.themoviedb.org/3/search/movie?api_key=aed84e419fb70252f967303e3cf406d2&query=${title}&language=es`
      )
    ).data.results;
  }
}
