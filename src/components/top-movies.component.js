import { LitElement, html } from 'lit';
import { TopMoviesUseCase } from '../usecases/top-movies.usecase';
import '../ui/movie.ui';

export class TopMoviesComponent extends LitElement {
  static get properties() {
    return {
      movies: { type: Array },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    const storedMovies = localStorage.getItem('movies');
    if (storedMovies) {
      this.movies = JSON.parse(storedMovies);
    } else {
      this.movies = await TopMoviesUseCase.execute();
      localStorage.setItem('movies', JSON.stringify(this.movies));
    }
  }

  render() {
    return html`
      <h1 class="title" id="titleSection">Top-Rated Movies</h1>
      <ul class="container-section">
        ${this.movies?.map(
          (movie) =>
            html`<li @click="${() => this.handleMovieClick(movie)}">
              <movie-ui .movie=${movie}></movie-ui>
            </li>`
        )}
      </ul>
    `;
  }

  handleMovieClick(movie) {
    const movieSelected = new CustomEvent('movie:selected', {
      detail: movie,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(movieSelected);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('top-movies-component', TopMoviesComponent);
