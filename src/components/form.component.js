import { LitElement, html } from 'lit';

export class FormComponent extends LitElement {
  connectedCallback() {
    super.connectedCallback();
    this.searchValue = '';
  }
  static get properties() {
    return {
      searchValue: { type: String },
    };
  }
  render() {
    return html`
      <form @submit="${this.search}" class="form">
        <label for="searchInput" class="form__label"
          >Search movie by title:</label
        >
        <div class="form__div">
          <input
            id="searchInput"
            type="text"
            placeholder="Search movie by title"
            aria-label="Search by movie title"
            .value="${this.searchValue}"
            @input="${this.setSearchValue}"
            class="form__input"
          />
          <button
            @submit="${this.search}"
            type="submit"
            id="searchButton"
            ?disabled=${!this.searchValue}
            class="form__button button"
          >
            Search
          </button>
        </div>
      </form>
    `;
  }

  search(e) {
    e.preventDefault();
    const search = new CustomEvent('search:title', {
      bubbles: true,
      composed: true,
      detail: this.searchValue,
    });
    this.dispatchEvent(search);
  }

  setSearchValue(e) {
    this.searchValue = e.target.value;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('form-component', FormComponent);
