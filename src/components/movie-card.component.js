import { LitElement, html } from 'lit';

export class MovieCardComponent extends LitElement {
  async handleMovieRecommendations(movie) {
    const recommendations = new CustomEvent('movie:recommendation', {
      detail: movie,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(recommendations);
  }

  render() {
    console.log(this.movie);
    return html`<section class="movie-section">
        <article class="movie">
          <img
            src="https://image.tmdb.org/t/p/w200/${this.movie?.poster}"
            alt="${this.movie?.title}"
            class="movie__image"
          />
          <h3 class="movie__title">${this.movie?.title}</h3>
          <div class="movie__text">
            <p>${this.movie?.date}</p>
            <p >&#9733 ${this.movie?.score}</p>
          </div>
          <p> ${this.movie?.overview}</p>
          <button class="button" @click="${() =>
            this.handleMovieRecommendations(
              this.movie
            )}">Películas similares</button>
        </article>
    </section>`;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define('movie-card-component', MovieCardComponent);
