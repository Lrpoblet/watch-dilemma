import { LitElement, html } from 'lit';
import { RecommendationsUseCase } from '../usecases/recommendations.usecase';
import '../ui/movie.ui';

export class RecommendationsComponent extends LitElement {
  static get properties() {
    return {
      movies: { type: Array },
      movieId: { type: Number },
      movieTitle: { type: String },
    };
  }

  async connectedCallback() {
    super.connectedCallback();
    this.movieId = '';
    this.movieTitle = '';
  }

  render() {
    return html`
      <h2 class="title" id="titleSection">
        Movies similar to: ${this.movieTitle}
      </h2>
      <ul class="container-section">
        ${this.movies?.map(
          (movie) =>
            html`<li @click="${() => this.handleMovieClick(movie)}">
              <movie-ui .movie=${movie}></movie-ui>
            </li>`
        )}
      </ul>
    `;
  }

  async handleSelectedMovie(movie) {
    this.movieId = movie.id;
    this.movieTitle = movie.title;
    this.movies = await RecommendationsUseCase.execute(this.movieId);
  }

  handleMovieClick(movie) {
    const movieSelected = new CustomEvent('movie:selected', {
      detail: movie,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(movieSelected);
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('recommendations-component', RecommendationsComponent);
