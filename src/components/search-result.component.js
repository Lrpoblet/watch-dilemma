import { LitElement, html } from 'lit';
import { SearchByNameUseCase } from '../usecases/search-by-name.usecase';
import '../ui/movie.ui';

export class SearchResultComponent extends LitElement {
  async connectedCallback() {
    super.connectedCallback();
    this.movies = [];
    this.titleValue = '';
  }

  static get properties() {
    return {
      movies: { type: Array },
      titleValue: { type: String },
    };
  }

  render() {
    return html`
      <h2 class="title" id="titleSection">Search result: ${this.titleValue}</h2>
      ${this.movies.length === 0
        ? html`<p class="not-results">No results found</p>
            <button @click="${this.back}" type="submit" class="button">
              Come back to Top-Rated Movies
            </button> `
        : html`
            <ul class="container-section">
              ${this.movies.map(
                (movie) => html`
                  <li @click="${() => this.handleMovieClick(movie)}">
                    <movie-ui .movie=${movie}></movie-ui>
                  </li>
                `
              )}
            </ul>
          `}
    `;
  }

  async handleSearchMovie(title) {
    this.titleValue = title;
    const searchResult = await SearchByNameUseCase.execute(title);
    this.movies = searchResult;
  }

  handleMovieClick(movie) {
    const movieSelected = new CustomEvent('movie:selected', {
      detail: movie,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(movieSelected);
  }

  back() {
    location.reload();
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define('search-result-component', SearchResultComponent);
