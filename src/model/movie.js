export class Movie {
  constructor({
    id,
    title,
    date,
    overview,
    poster,
    score,
    backgroundImage,
    genre,
    language,
  }) {
    this.id = id;
    this.title = title;
    this.date = date;
    this.overview = overview;
    this.poster = poster;
    this.score = score;
    this.backgroundImage = backgroundImage;
    this.genre = genre;
    this.language = language;
  }
}
