import '../components/form.component';
import '../components/top-movies.component';
import '../components/search-result.component';
import '../components/recommendations.component';
import '../components/movie-card.component';

export class HomePage extends HTMLElement {
  static get properties() {
    return {
      movie: { type: Object },
    };
  }

  constructor() {
    super();
  }

  connectedCallback() {
    this.innerHTML = `
    <form-component></form-component>
    <top-movies-component></top-movies-component>
  `;

    this.addEventListener('search:title', (e) => {
      this.innerHTML = `
        <form-component></form-component>
        <search-result-component id="searchResult"></search-result-component>`;
      const searchResult = this.querySelector('#searchResult');
      searchResult.handleSearchMovie(e.detail);
    });

    this.addEventListener('movie:recommendation', (e) => {
      this.innerHTML = `
        <form-component></form-component>
        <recommendations-component id="recommendations"></recommendations-component>`;
      const recommendations = this.querySelector('#recommendations');
      recommendations.handleSelectedMovie(e.detail);
    });

    this.addEventListener('movie:selected', (e) => {
      const movieCard = document.createElement('movie-card-component');
      movieCard.movie = e.detail;
      this.innerHTML = '';
      this.appendChild(document.createElement('form-component'));
      this.appendChild(movieCard);
    });
  }
}

customElements.define('home-page', HomePage);
