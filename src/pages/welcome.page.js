import { Router } from '@vaadin/router';

export class WelcomePage extends HTMLElement {
  connectedCallback() {
    this.innerHTML = `
        <p class="header__text">
          Tired of wasting time browsing through endless movie catalogs? Feeling
          indecisive and not sure what to watch? Your solution has arrived! With
          this app, you can easily access the top-rated movies, as well as
          search for a movie you liked and discover other similar movie
          recommendations by clicking on it. Don't waste any more time, start searching and find
          your next favorite movie!
        </p>
        <button class="button" id="goToHomePage">Let's go</button>
      `;

    const button = this.querySelector('#goToHomePage');
    button.addEventListener('click', () => {
      Router.go('/home');
    });
  }
}

customElements.define('welcome-page', WelcomePage);
