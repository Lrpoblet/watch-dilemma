import { LitElement, html } from 'lit';
import './../components/top-movies.component';
import './../components/search-result.component';
import '../components/recommendations.component';

export class MovieUI extends LitElement {
  static get properties() {
    return {
      movie: { type: Object },
    };
  }

  async handleMovieRecommendations(movie) {
    const recommendations = new CustomEvent('movie:recommendation', {
      detail: movie,
      bubbles: true,
      composed: true,
    });
    this.dispatchEvent(recommendations);
  }

  render() {
    return html`<article class="movie">
      <img
        src="https://image.tmdb.org/t/p/w200/${this.movie?.poster}"
        alt="${this.movie?.title}"
        class="movie__image"
      />
      <h3 class="movie__title">${this.movie?.title}</h3>
      <div class="movie__text">
        <p>${this.movie?.date}</p>
        <p >&#9733 ${this.movie?.score}</p>
      </div>
      <button class="button" @click="${() =>
        this.handleMovieRecommendations(
          this.movie
        )}">Películas similares</button>
    </article>`;
  }

  createRenderRoot() {
    return this;
  }
}
customElements.define('movie-ui', MovieUI);
