import './styles/core/reset.css';
import './styles/core/variables.css';
import './styles/components/animation.css';
import './styles/components/button.css';
import './styles/components/not-results.css';
import './styles/components/titles.css';
import './styles/main.css';
import './styles/layout/header.css';
import './styles/layout/form.css';
import './styles/layout/movies.css';
import './styles/layout/footer.css';

import './pages/welcome.page';
import './pages/home.page';

import { Router } from '@vaadin/router';

const outlet = document.getElementById('outlet');
const router = new Router(outlet);

router.setRoutes([
  { path: '/welcome', component: 'welcome-page' },
  { path: '/home', component: 'home-page' },
  { path: '(.*)', redirect: '/welcome' },
]);
