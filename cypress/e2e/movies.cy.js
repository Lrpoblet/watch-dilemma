/// <reference types="Cypress" />

describe('Watch Dilemma', () => {
  beforeEach(() => {
    cy.visit('/');
    cy.wait(1000);
  });

  it('search button is disabled until input is entered', () => {
    cy.get('#searchButton').should('be.disabled');
    cy.get('#searchInput').type('kairos');
    cy.get('#searchButton').should('not.be.disabled');
  });

  it('user search movie by name', () => {
    cy.get('#searchInput').should('have.value', '');
    cy.get('#titleSection').contains('Top-Rated Movies');
    cy.get(':nth-child(1) > movie-ui > article > h3').contains('The Godfather');
    cy.get('#searchInput').type('kairós');
    cy.get('#searchInput').should('have.value', 'kairós');
    cy.get('#searchButton').click();
    cy.get('h2').contains('Search result: kairós');
    cy.get(':nth-child(1) > movie-ui > article > h3').contains('Kairos');
  });

  it('user search movie by pressing Enter key', () => {
    cy.get('#searchInput').should('have.value', '');
    cy.get('#searchInput').type('kairós{enter}');
    cy.get('#titleSection').contains('Search result: kairós');
    cy.get(':nth-child(1) > movie-ui > article > h3').contains('Kairos');
  });

  it(`user search movie but it doesn't exist`, () => {
    cy.get('#searchInput').should('have.value', '');
    cy.get('#searchInput').type('sadfas');
    cy.get('#searchInput').should('have.value', 'sadfas');
    cy.get('#searchButton').click();
    cy.get('#titleSection').contains('Search result: sadfas');
    cy.get('#searchResult > p').contains('No results found');
  });

  it('user click post and see recommendations', () => {
    cy.get(':nth-child(1) > movie-ui > article').click();
    cy.get('#titleSection').contains('Movies similar to: The Godfather');
    cy.get(':nth-child(1) > movie-ui > article > h3').contains(
      'Cuando Sea Joven'
    );
  });
});
